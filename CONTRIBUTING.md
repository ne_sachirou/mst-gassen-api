Install
--
Install Git, PHP >=5.4 and Composer.
```sh
git clone https://ne_sachirou@bitbucket.org/ne_sachirou/mst-gassen-api.git
cd mst-gassen-api
composer install
MY_LARAVEL_ENV=local ./artisan serve
```

Test
--
```sh
vendor/bin/phpunit
```
