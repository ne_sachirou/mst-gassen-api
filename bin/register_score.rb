require 'net/http'
require 'uri'
require 'bundler'
Bundler.require

def register user_name, score
  puts "#{user_name} #{score}"
  # uri = URI.parse 'http://192.168.137.10:3000/api/register_score'
  uri = URI.parse 'http://localhost:3000/api/register_score'
  res = Net::HTTP.post_form uri, user_name: user_name, score: score
  puts res.body
end

locales = ['de-AT', 'de-CH', :de, 'en-AU', 'en-BORK', 'en-CA', 'en-GB', 'en-IND', 'en-US', 'en-au-ocker', :en, :es, :fa, :fr, :it, :ja, :ko, 'nb-NO', :nl, :pl, 'pt-BR', :ru, :sk, :sv, :vi, 'zh-CN']
locales = [:ja]
while true
  Faker::Config.locale = locales.sample
  user_name = Faker::Name.name
  score     = rand 0..27
  register user_name, score
  sleep 0.5

  # user_name = Random.new.bytes rand(4..12)
  # user_name = 12.times.map{ '<>&"\'/!?'.split('').sample }.to_a.join ''
  # score     = rand 0..27
  # register user_name, score
  # sleep 0.5
end
