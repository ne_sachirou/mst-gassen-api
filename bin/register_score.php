<?php
$chars = [
  'あ', 'い', 'う', 'え', 'お',
  'か', 'き', 'く', 'け', 'こ',
  'さ', 'し', 'す', 'せ', 'そ',
  'た', 'ち', 'つ', 'て', 'と',
  'な', 'に', 'ぬ', 'ね', 'の',
  'は', 'ひ', 'ふ', 'へ', 'ほ',
  'ま', 'み', 'む', 'め', 'も',
  'や',       'ゆ',       'よ',
  'ら', 'り', 'る', 'れ', 'ろ',
  'わ', 'ゐ',       'ゑ', 'を', 'ん', '　',
];
while (true) {
    $userName = '';
    for ($i = 0, $iz = mt_rand(4, 12); $i < $iz; ++$i) {
      $userName .= $chars[mt_rand(0, count($chars) - 1)];
    }
    $score = mt_rand(0, 27);
    echo "$userName $score\n";
    $data = http_build_query(
      ['user_name' => $userName, 'score' => $score],
      '',
      '&'
    );
    $res = file_get_contents(
      'http://localhost:3000/api/register_score',
      false,
      stream_context_create([
        'http' => [
            'method' => 'POST',
            'header' => [
                'Content-Type: application/x-www-form-urlencoded',
                'Content-Length: '.strlen($data),
            ],
            'content' => $data,
        ],
      ])
    );
    echo "$res\n";
    sleep(0.5);
}
