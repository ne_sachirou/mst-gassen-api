mst-gassen-api
==
MST2014 Gassen's API component.

Use PHP Laravel4 framwork.

Install
--
Install Git, PHP >=5.4 and Composer.
```sh
git clone https://ne_sachirou@bitbucket.org/ne_sachirou/mst-gassen-api.git
cd mst-gassen-api
composer install
MY_LARAVEL_ENV=local ./artisan migrate
MY_LARAVEL_ENV=local ./artisan serve --host 0.0.0.0 --port 3000
MY_LARAVEL_ENV=local ./artisan ws:serve --port 3030
watch -d -n2 'http GET localhost:3000/api/ranking | jq -c ".[]|{user_name:.user.user_name,score:.score}" | head -n40 | sed "s/\([,:]\)/\1\t/g"'
```

Access to /ranking/ on your Web browser.

To test to update ranking.
```sh
ruby bin/register_score.rb
```
or,
```sh
php bin/register_score.php
```

API
--
### GET /api/register_score
Test用にPOST /api/register_scoreへPOSTするform。

Response:

- _html_ HTML

### POST /api/register_score
Demo用にsocoreを記録する。

Param:

- **user_name** required UTF8string max:50
- **score** required integer min:0

Response:

- _json_ {ok:ok}

### GET /api/ranking
Demo用のrankingを取得。

Param:

- **type** optional 'html'|'json' default:'json'

Response:

- _html_ HTML
- _json_ [{score:INT,user:{user_name:STR}}]
