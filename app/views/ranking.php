<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8"/>
	<title>番付</title>
	<link rel="stylesheet" href="/ranking/ranking.css"/>
</head>
<body>
	<div id="ranking"></div>
	<template id="ranking-item">
		<div class="ranking-item" data-user-id="{{userId}}">
			<div class="ranking-item-order">{{order}}</div>
			<div class="ranking-item-userName">{{userName}}</div>
			<div class="ranking-item-score">{{score}}</div>
		</div>
	</template>
	<script src="/ranking/ranking.js"></script>
	<script>
		document.addEventListener('DOMContentLoaded', function () {
			var datas = '<?php echo htmlspecialchars($ranking, ENT_QUOTES, 'UTF-8'); ?>';

			datas = datas.
				replace('&lt;', '<').
				replace('&gt;', '>').
				replace('&quot;', '"').
				replace('&#039;', "'").
				replace('&amp;', '&');
			try {
				datas = JSON.parse(datas);
			} catch (err) {
				datas = void 0;
			}
			new RankingApp('#ranking').start();
		});
	</script>
</body>
</html>
