<!doctype html>
<html lang="ja">
<head>
	<meta charset="UTF-8"/>
	<title>Register Score</title>
</head>
<body>
	<form action="/api/register_score" method="POST">
		<div>
			<span>user_name</span>
			<input name="user_name" type="text" required maxlen="50" size="50"/>
		</div>
			<span>score</span>
			<input name="score" type="number" required min="0" max="99" size="3"/>
		<div>
		</div>
			<input type="submit" value="Register"/>
		<div>
		</div>
	</form>
	<hr/>
	<form action="/api/ranking" method="POST">
		<input type="submit" value="Reset Ranking"/>
		<input name="_method" type="hidden" value="DELETE"/>
	</form>
</body>
</html>
