<?php

class TestCase extends Illuminate\Foundation\Testing\TestCase {

	/**
	 * Creates the application.
	 *
	 * @return \Symfony\Component\HttpKernel\HttpKernelInterface
	 */
	public function createApplication()
	{
		$unitTesting = true;

		$testEnvironment = 'testing';

		return require __DIR__.'/../../bootstrap/start.php';
	}

  public function setUp()
  {
    parent::setUp();
    $this->setUpDb();
  }

  public function teardown()
  {
    $this->tearDownDb();
  }

  public function setUpDb()
  {
    Artisan::call('migrate');
    Artisan::call('db:seed');
  }

  public function teardownDb()
  {
    Artisan::call('migrate:reset');
  }

}
