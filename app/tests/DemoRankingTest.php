<?php

class DemoRankingTest extends TestCase {
  public function testCheckUser() {
    $user1 = User::createForDemonstration('ももんが');
    $user2 = User::createForDemonstration('百々ん蛾');
    $user3 = User::createForDemonstration('ももんが王');
    $user1->insertNewGame(42);
    $user1->insertNewGame(42);
    $user2->insertNewGame(42);
    sleep(1);
    $user3->insertNewGame(42);
    $ranks = DemoRanking::getRanking();
    $this->assertEquals($user1->user_id, $ranks[0]->user_id);
    $this->assertEquals(84,              $ranks[0]->score);
    $this->assertEquals($user3->user_id, $ranks[1]->user_id);
    $this->assertEquals(42,              $ranks[1]->score);
    $this->assertEquals($user2->user_id, $ranks[2]->user_id);
    $this->assertEquals(42,              $ranks[2]->score);
  }

  public function testCheckUserOverflow() {
    for ($i = 0; $i < 200; ++$i) {
      $user = User::createForDemonstration("$i ももんが");
      $user->insertNewGame($i - $i % 2);
    }
    $this->assertEquals(100, DemoRanking::all()->count());
    $ranks = DemoRanking::getRanking();
    $this->assertEquals(100, count($ranks));
    $scores = [];
    foreach ($ranks as $rank) {
      $scores[] = intval($rank->score);
    }
    $sortedScores = $scores;
    rsort($sortedScores);
    $this->assertEquals($sortedScores, $scores);
  }
}
