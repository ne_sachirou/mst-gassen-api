<?php

class UserTest extends TestCase {
  public function testCreateForDemonstration() {
    $userName = 'ももんが 百々ん蛾';
    $user = User::createForDemonstration($userName);
    $this->assertEquals($userName, $user->user_name);
    $this->assertFalse($user->isDirty());
  }

  public function testInsertNewGame() {
    $user = User::createForDemonstration('ももんが');
    $user->insertNewGame(42);
    $user->insertNewGame(42);
    $games = $user->demoGames()->get();
    $this->assertEquals(2, count($games));
    $this->assertEquals(42, $games[0]->score);
    $this->assertEquals(42, $games[1]->score);
  }

  public function testScore() {
    $user = User::createForDemonstration('ももんが');
    $user->insertNewGame(34);
    $user->insertNewGame(8);
    $this->assertEquals(42, $user->score());
  }
}
