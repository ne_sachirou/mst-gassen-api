<?php

class DemoApiController extends BaseController {
	public function getRegisterScore() {
		return View::make('register_score');
	}

	public function registerScore() {
		Validator::extend('utf8', function ($attr, $value, $params) {
			return mb_check_encoding($value, 'UTF-8');
		});
		$validator = Validator::make(
			Input::all(),
			[
				'user_name' => 'required|utf8|max:50',
				'score'     => 'required|integer|min:0|max:99',
			]
		);
		if ($validator->fails()) {
			return Response::make(json_encode($validator->messages()), 400)
					->header('Content-Type', 'application/json');
		}
		$userName = Input::get('user_name');
		$score = intval(Input::get('score'));
		$user = User::createForDemo($userName);
		$user->insertNewGame($score);
		$this->notifyToUpdateRanking();
		return Response::make(json_encode(['ok' => 'ok']), 200)
			->header('Content-Type', 'application/json');
	}

	public function ranking() {
		$type = Input::get('type');
		if ('html' !== $type) { $type = 'json'; }
		$ranking = json_encode(DemoRanking::getRanking()->toArray());
		switch ($type) {
			case 'html':
				return View::make('ranking', ['ranking' => $ranking]);
			case 'json':
			default:
				return Response::make($ranking, 200)
					->header('Content-Type', 'application/json');
		}
	}

	public function resetRanking() {
		DemoGame::truncate();
		DemoRanking::truncate();
		User::truncate();
		$this->notifyToUpdateRanking();
		return Response::make(json_encode(['ok' => 'ok']), 200)
			->header('Content-Type', 'application/json');
	}

	private function notifyToUpdateRanking()
	{
		$ranking = DemoRanking::getRanking()->toArray();
		$context = new ZMQContext();
		$socket = $context->getSocket(ZMQ::SOCKET_PUSH, 'my pusher');
		$socket->connect('tcp://localhost:5555');
		$socket->send(json_encode([
			'message' => 'Update ranking.',
			'ranking' => $ranking,
		]));
	}
}
