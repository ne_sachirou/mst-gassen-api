<?php
use Ratchet\ConnectionInterface;
use Ratchet\Http\HttpServer;
use Ratchet\MessageComponentInterface;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;

class WsApp implements MessageComponentInterface
{
	static public function run($port)
	{
		$app = new self;
		$server = IoServer::factory(new HttpServer(new WsServer($app)), $port);
		App::instance('wsApp', $app);
		App::instance('wsServer', $server);
		(new WatchQueue)->start();
		$server->run();
	}

	protected $clients;

	public function __construct()
	{
		$this->clients = new \SplObjectStorage;
	}

	public function onOpen(ConnectionInterface $conn)
	{
		$this->clients->attach($conn);
	}

	public function onMessage(ConnectionInterface $from, $msg)
	{
		var_dump($msg);
	}

	public function onClose(ConnectionInterface $conn)
	{
		$this->clients->detach($conn);
	}

	public function onError(ConnectionInterface $conn, \Exception $ex)
	{
		echo ((string) $ex)."\n";
		$conn->close();
	}

	/**
	 * @param string $msg
	 * @return void
	 */
	public function broadcast($msg)
	{
		foreach ($this->clients as $client) {
			$client->send($msg);
		}
	}
}
