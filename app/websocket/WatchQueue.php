<?php

class WatchQueue
{
	public function start()
	{
		$this->run();
	}

	public function run()
	{
		$loop = App::make('wsServer')->loop;
		$context = new React\ZMQ\Context($loop);
		$pull = $context->getSocket(ZMQ::SOCKET_PULL);
		$pull->bind('tcp://127.0.0.1:5555');
		$pull->on('message', function ($msg) {
			$this->checkMessage($msg);
		});
	}

	private function checkMessage($msg)
	{
		$msg = json_decode($msg, true);
		if (!$msg
			|| !isset($msg['message'])
			|| 'Update ranking.' !== $msg['message']) {
			return;
		}
		$ranking = $msg['ranking'];
		App::make('wsApp')->broadcast(json_encode([
			'message' => 'Update ranking.',
			'ranking' => $ranking,
		]));
	}
}
