<?php

class UserClass extends Eloquent {
	public $timestamps = false;

	protected $table = 'class';
	protected $primaryKey = 'class_id';

	public function users() { return $this->hasMany('User'); }
}
