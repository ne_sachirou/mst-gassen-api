<?php

class User extends Eloquent {
	/**
	 * @param string $user_name
	 * @return User
	 */
	static public function createForDemo($userName) {
		if (!($class = UserClass::find(1))) {
			$class = new UserClass;
			$class->class_id = 1;
			$class->class_name = '百々ん蛾';
			$class->save();
		}
		$user = new User;
		$user->user_name = $userName;
		$user->password = '8u!D*uEF5k4I49CTZbTPcfG*';
		$user->coin = 0;
		$user->birthday = '1970-01-01';
		$user->mail_address = 'example@tokyo.hal.ac.jp';
		$user->delete_flag = '0';
		$user->class_id = $class->class_id;
		DB::transaction(function () use (&$user) {
			$user->user_id = DB::table('user')->max('user_id') + 1;
			$user->save();
		});
		return $user;
	}

	public $timestamps = false;

	protected $table = 'user';
	protected $primaryKey = 'user_id';
	protected $hidden = ['password'];

	public function userClass() { return $this->belongsTo('UserClass'); }

	public function demoGames() { return $this->hasMany('DemoGame'); }

	public function demoRanking() { return $this->hasOne('DemoRanking'); }

	/**
	 * @return int
	 */
	public function score() {
		$score = 0;
		foreach ($this->demoGames()->get() as $game) {
			$score += intval($game->score);
		}
		return $score;
	}

	/**
	 * @param int $score
	 * @return void
	 */
	public function insertNewGame($score) {
		DemoGame::create(['user_id' => $this->user_id, 'score' => $score]);
		DemoRanking::checkUser($this);
	}
}
