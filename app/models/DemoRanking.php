<?php

class DemoRanking extends Eloquent {
	/** @var int */
	static private $maxCount = 100;

	/**
	 * @param User $user
	 * @return void
	 */
	static public function checkUser(User $user) {
		$score = $user->score();
		if ($score <= 0) { return; }
		if ($rank = self::where('user_id', $user->user_id)->first()) {
			if ($rank->score < $score) {
				$rank->update(['score' => $score]);
			}
			return;
		}
		self::create(['user_id' => $user->user_id, 'score' => $score]);
		if (self::all()->count() <= self::$maxCount) { return; }
		DB::transaction(function () {
			$count = self::all()->count();
			foreach (self::where('created_at', '<', 'now - 30 min')->get() as $rank) {
				if ($count <= self::$maxCount) { return; }
				$rank->delete();
				--$count;
			}
			$minScore = self::orderBy('score', 'desc')
				->skip(self::$maxCount - 1)
				->first()
				->score;
			foreach (self::where('score', '<', $minScore)->get() as $rank) {
				if ($count <= self::$maxCount) { return; }
				$rank->delete();
				--$count;
			}
		});
	}

	static public function getRanking()
	{
		return self::with('user')
			->orderBy('score', 'desc')
			->orderBy('updated_at', 'desc')
			->take(self::$maxCount)
			->get();
	}

	protected $table = 'demo_rankings';
	protected $fillable = ['user_id', 'score'];

	public function user() { return $this->belongsTo('User'); }
}
