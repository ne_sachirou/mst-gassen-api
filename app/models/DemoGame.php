<?php

class DemoGame extends Eloquent {
	protected $table = 'demo_games';
	protected $fillable = ['user_id', 'score'];

	public function user() { return $this->belongsTo('User'); }
}
