<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInitialTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
    Schema::create('category', function ($t) { // カテゴリー
      $t->integer('category_id');
      $t->string('category_name', 50);
      $t->string('category_describe', 280)->nullable()->default(null); // カテゴリーの説明
      $t->primary('category_id');
    });
    DB::table('category')->insert([
      [
        'category_id'       => 1,
        'category_name'     => '武器-KATANA',
        'category_describe' => '斬ります',
      ],
      [
        'category_id'       => 2,
        'category_name'     => '武器-槍',
        'category_describe' => '突きます',
      ],
      [
        'category_id'       => 3,
        'category_name'     => '武器-銃',
        'category_describe' => '撃てます',
      ],
      [
        'category_id'       => 4,
        'category_name'     => '鎧',
        'category_describe' => '身を守ります',
      ],
      [
        'category_id'       => 5,
        'category_name'     => '兜',
        'category_describe' => '頭を守ります',
      ],
      [
        'category_id'       => 6,
        'category_name'     => 'アクセサリ',
        'category_describe' => '着飾ります',
      ],
    ]);

    Schema::create('class', function ($t) { // 階級
      $t->integer('class_id');
      $t->string('class_name', 50);
      $t->string('class_describe', 280)->nullable()->default(null); // 階級の説明
      $t->primary('class_id');
    });

    Schema::create('gassen', function ($t) { // 合戦
      $t->integer('gassen_id');
      $t->integer('site_id');
      $t->integer('eastleader_id');
      $t->integer('westleader_id');
      $t->date('battle_day');
      $t->time('outbreak_time');
      $t->time('end_time');
      $t->primary('gassen_id');
    });

    Schema::create('item', function ($t) { // アイテム
      $t->integer('item_id');
      $t->string('item_name', 50);
      $t->string('item_describe', 280)->nullable()->default(null);
      $t->binary('item_image')->nullable();
      $t->integer('price');
      $t->integer('dl_count');
      $t->integer('category_id')->references('category_id')->on('category');
      $t->string('delete_flag', 1);
      $t->primary('item_id');
    });
    DB::table('item')->insert([
      [
        'item_id'       => 1,
        'item_name'     => '名刀・残月',
        'item_describe' => '月をも斬ることができると言い伝えられる伝説の刀。太刀筋が残像として発生する特殊な造りであり その残像が月のように見えることからその名がついた。',
        'item_image'    => NULL,
        'price'         => 1000,
        'dl_count'      => 10,
        'category_id'   => 1,
        'delete_flag'   => '0',
      ],
      [
        'item_id'       => 2,
        'item_name'     => 'ドラゴンキラー',
        'item_describe' => '竜の固い鱗を裂くことに特化した剣。',
        'item_image'    => NULL,
        'price'         => 800,
        'dl_count'      => 25,
        'category_id'   => 1,
        'delete_flag'   => '0',
      ],
      [
        'item_id'       => 3,
        'item_name'     => 'パラレルショットガン',
        'item_describe' => '銃口が並列に二つ並んだ玄人向けのショットガン。',
        'item_image'    => NULL,
        'price'         => 600,
        'dl_count'      => 20,
        'category_id'   => 3,
        'delete_flag'   => '0',
      ],
      [
        'item_id'       => 4,
        'item_name'     => '中野の兜',
        'item_describe' => '名将・中野裕貴が使用していたとされる至高の安兜。',
        'item_image'    => NULL,
        'price'         => 300,
        'dl_count'      => 1,
        'category_id'   => 5,
        'delete_flag'   => '0',
      ],
    ]);

    Schema::create('team', function ($t) { // チーム
      $t->integer('gassen_id')->references('gassen_id')->on('gassen');
      $t->string('team_side', 1); // 東 / 西
      $t->integer('user_id');
      $t->primary(['gassen_id', 'team_side', 'user_id']);
    });

    Schema::create('the_site', function ($t) { // 開催地
      $t->integer('site_id');
      $t->string('prefectures', 50); // 都道府県
      $t->string('municipalities', 50); // 市町村
      $t->primary('site_id');
    });
    DB::table('the_site')->insert([
      ['site_id' => 1, 'prefectures' => '東京都', 'municipalities' => 'A区' ],
      ['site_id' => 2, 'prefectures' => '東京都', 'municipalities' => 'B区' ],
      ['site_id' => 3, 'prefectures' => '東京都', 'municipalities' => 'C区' ],
      ['site_id' => 4, 'prefectures' => '東京都', 'municipalities' => 'D区' ],
    ]);

    Schema::create('user', function ($t) { // ユーザー
      $t->integer('user_id');
      $t->string('user_name', 50);
      $t->string('password', 16);
      $t->integer('coin');
      $t->date('birthday');
      $t->string('mail_address', 100);
      $t->string('delete_flag', 1);
      $t->integer('equipment1')->nullable()->default(null);
      $t->integer('equipment2')->nullable()->default(null);
      $t->integer('equipment3')->nullable()->default(null);
      $t->integer('equipment4')->nullable()->default(null);
      $t->integer('class_id')->references('class_id')->on('class');
      $t->integer('helm_hunt_count')->nullable()->default(null); // 狩ったことのある兜数
      $t->primary('user_id');
    });

    Schema::create('user_property', function ($t) { // ユーザーの所持アイテム
      $t->integer('user_id');
      $t->integer('item_id')->references('item_id')->on('item');
      $t->integer('count'); // アイテムの所持数
      $t->primary(['user_id', 'item_id']);
    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
    Schema::drop('user_property');
    Schema::drop('user');
    Schema::drop('the_site');
    Schema::drop('team');
    Schema::drop('item');
    Schema::drop('gassen');
    Schema::drop('class');
    Schema::drop('category');
	}

}
