<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDemoRankingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
    Schema::create('demo_rankings', function ($t) {
      $t->increments('id');
      $t->integer('user_id')->references('user_id')->on('user');
      $t->integer('score')->unsigned();
      $t->timestamps();
    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
    Schema::drop('demo_rankings');
	}

}
