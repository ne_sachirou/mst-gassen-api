-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- ホスト: localhost
-- 生成時間: 2014 年 10 月 27 日 16:25
-- サーバのバージョン: 5.5.16
-- PHP のバージョン: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- データベース: `thunderbolt`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(50) NOT NULL,
  `category_describe` varchar(280) DEFAULT NULL COMMENT 'カテゴリーの説明',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='カテゴリー';

--
-- テーブルのデータをダンプしています `category`
--

INSERT INTO `category` (`category_id`, `category_name`, `category_describe`) VALUES
(1, '武器-KATANA', '斬ります'),
(2, '武器-槍', '突きます'),
(3, '武器-銃', '撃てます'),
(4, '鎧', '身を守ります'),
(5, '兜', '頭を守ります'),
(6, 'アクセサリ', '着飾ります');

-- --------------------------------------------------------

--
-- テーブルの構造 `class`
--

CREATE TABLE IF NOT EXISTS `class` (
  `class_id` int(11) NOT NULL,
  `class_name` varchar(50) NOT NULL,
  `class_describe` varchar(280) DEFAULT NULL COMMENT '階級の説明',
  PRIMARY KEY (`class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='階級';

-- --------------------------------------------------------

--
-- テーブルの構造 `gassen`
--

CREATE TABLE IF NOT EXISTS `gassen` (
  `gassen_id` int(11) NOT NULL,
  `site_id` int(11) NOT NULL,
  `eastleader_id` int(11) NOT NULL,
  `westleader_id` int(11) NOT NULL,
  `battle_day` date NOT NULL,
  `outbreak_time` time NOT NULL,
  `end_time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='合戦';

-- --------------------------------------------------------

--
-- テーブルの構造 `item`
--

CREATE TABLE IF NOT EXISTS `item` (
  `item_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `item_describe` varchar(280) NOT NULL,
  `item_image` blob,
  `delete_flag` varchar(1) NOT NULL,
  `dl_count` int(11) NOT NULL,
  `item_name` varchar(50) NOT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='アイテム';

--
-- テーブルのデータをダンプしています `item`
--

INSERT INTO `item` (`item_id`, `category_id`, `price`, `item_describe`, `item_image`, `delete_flag`, `dl_count`, `item_name`) VALUES
(1, 1, 1000, '月をも斬ることができると言い伝えられる伝説の刀。太刀筋が残像として発生する特殊な造りであり その残像が月のように見えることからその名がついた。', NULL, '0', 10, '名刀・残月'),
(2, 1, 800, '竜の固い鱗を裂くことに特化した剣。', NULL, '0', 25, 'ドラゴンキラー'),
(3, 3, 600, '銃口が並列に二つ並んだ玄人向けのショットガン。', NULL, '0', 20, 'パラレルショットガン'),
(4, 5, 300, '名将・中野裕貴が使用していたとされる至高の安兜。', NULL, '0', 1, '中野の兜');

-- --------------------------------------------------------

--
-- テーブルの構造 `team`
--

CREATE TABLE IF NOT EXISTS `team` (
  `gassen_id` int(11) NOT NULL,
  `team_side` varchar(1) NOT NULL COMMENT '東 / 西',
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`gassen_id`,`team_side`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='チーム';

-- --------------------------------------------------------

--
-- テーブルの構造 `the_site`
--

CREATE TABLE IF NOT EXISTS `the_site` (
  `site_id` int(11) NOT NULL,
  `prefectures` varchar(50) NOT NULL COMMENT '都道府県',
  `municipalities` varchar(50) NOT NULL COMMENT '市町村',
  PRIMARY KEY (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='開催地';

--
-- テーブルのデータをダンプしています `the_site`
--

INSERT INTO `the_site` (`site_id`, `prefectures`, `municipalities`) VALUES
(1, '東京都', 'A区'),
(2, '東京都', 'B区'),
(3, '東京都', 'C区'),
(4, '東京都', 'D区');

-- --------------------------------------------------------

--
-- テーブルの構造 `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `password` varchar(16) NOT NULL,
  `coin` int(11) NOT NULL,
  `birthday` date NOT NULL,
  `mail_address` varchar(100) NOT NULL,
  `delete_flag` varchar(1) NOT NULL,
  `equipment1` int(11) DEFAULT NULL,
  `equipment2` int(11) DEFAULT NULL,
  `equipment3` int(11) DEFAULT NULL,
  `equipment4` int(11) DEFAULT NULL,
  `class_id` int(11) NOT NULL,
  `helm_hunt_count` int(11) DEFAULT NULL COMMENT '狩ったことのある兜数',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ユーザー';

-- --------------------------------------------------------

--
-- テーブルの構造 `user_history`
--

CREATE TABLE IF NOT EXISTS `user_history` (
  `user_id` int(11) NOT NULL,
  `gassen_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`gassen_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ユーザーの戦歴';

-- --------------------------------------------------------

--
-- テーブルの構造 `user_property`
--

CREATE TABLE IF NOT EXISTS `user_property` (
  `user_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `count` int(11) NOT NULL COMMENT 'アイテムの所持数',
  PRIMARY KEY (`user_id`,`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ユーザーの所持アイテム';

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
