<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/api/register_score', 'DemoApiController@getRegisterScore');

Route::post('/api/register_score', 'DemoApiController@registerScore');

Route::get('/api/ranking', 'DemoApiController@ranking');

Route::delete('/api/ranking', 'DemoApiController@resetRanking');
